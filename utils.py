from datetime import datetime

class column_formatting():
    def formatting_date(
        date
    ):
        try:
            new_date = datetime.strptime(date, '%d-%m-%Y').date()
        except:
            try:
                new_date = datetime.strptime(date, '%Y-%m-%d').date()
            
            except:
                try:
                    new_date = datetime.strptime(date, '%d-%b-%Y').date()
                except:
                    new_date = None
        return new_date
    
    def getting_po_reference(
        po_ref
    ):
        po_ref = str(po_ref)
        if po_ref.lower().startswith('po '):
            aux = int(po_ref.split(' ')[1])
            return 'po_pallet_reference', aux

        elif po_ref.lower().startswith('po'):
            aux = int(po_ref[2:])
            return 'po_pallet_reference', aux
        
        elif int(po_ref)<1000:
            return 'po_pallet_reference', int(po_ref)

        else:
            return 'pallet_reference', po_ref
        
        
        
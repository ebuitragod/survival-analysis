import numpy as np
import pandas as pd

from datetime import datetime

from constants import halls_inputs
from utils import column_formatting as utl



class cleaning_dataframes:
    
    def drymatter_cleaning(
        df
    ):
        drop_nas = [
            'origin',
            'arrival_date',
            'po_reference',
        ]
        df = df.dropna(subset=drop_nas)
        df['pallet_reference'] = np.nan
        df['po_pallet_reference'] = np.nan

        for index, row in df.iterrows():
            arrival_date = str(row['arrival_date']).split(' ')[0].replace(',', '-')
            df.loc[index, 'arrival_date'] = utl.formatting_date(
                arrival_date
            )

            sample_date = str(row['sample_date']).split(' ')[0]
            df.loc[index, 'sample_date'] = utl.formatting_date(
                sample_date
            )
            
            column_name, po_ref = utl.getting_po_reference(
                po_ref = row['po_reference']
            )
            df.loc[index, column_name] = po_ref

        df['pallet_reference'] = df.pallet_reference.astype('int64')
        df.info()
        return df

    
    def qc_summary_cleaning(
        df
    ):
        drop_nas = [
            'arrival_date',
            'po_reference',
            'origin',
            'score',
        ]
        df = df.dropna(subset=drop_nas)
        df['po_reference'] = df.po_reference.astype('int32')
        
        for index, row in df.iterrows():
            column_name, po_ref = utl.getting_po_reference(
                po_ref = row['po_reference']
            )
            df.loc[index, column_name] = po_ref
        df.info()
        return df
    
    def allocations_cleaning(
        df
    ):
        drop_nas = [
            'origin',
            'pallet_ref_cc',
            #'po_reference',
            'cell_date',
            'arrival_date'
        ]
        
        df = df.dropna(subset=drop_nas)
        df['pallet_ref_cc'] = df.pallet_ref_cc.astype('int64')
        
        for index, row in df.iterrows():
            column_name, po_ref = utl.getting_po_reference(
                po_ref = row['pallet_ref_cc']
            )
            df.loc[index, column_name] = po_ref
            
            arrival_date = row['arrival_date']+'-2023'
            df.loc[index, 'arrival_date'] = utl.formatting_date(
                arrival_date
            )
            allocated_date = row['allocated_date']+'-2023'
            df.loc[index, 'allocated_date'] = utl.formatting_date(
                allocated_date
            )
            cell_date = row['cell_date'].replace('/', '-')+'-2023'
            df.loc[index, 'cell_date'] = utl.formatting_date(
                cell_date
            )
        df.info()
        return df
        
        



def read_doc(
    file
):
    """
    chose between:
        file = 'allocations',
        file = 'qc_summary',
        file = 'drymatter',
        file = 'weekly_ripe',
    """
    doc = pd.DataFrame()
    try:
        file_atr = [fn for fn in halls_inputs if fn['file'] == file][0]
        extension = file_atr['extension']
        print(file_atr['file_name'])
        if extension == 'csv':
            doc = pd.read_csv(file_atr['file_name'])
        elif extension == 'xlsx':
            doc = pd.read_excel(file_atr['file_name'])
        else:
            print(f'document not found')
            
        columns_row = file_atr['columns_row']
        if columns_row is not None:
            doc.columns = doc.iloc[columns_row]
            doc = doc.iloc[pd.RangeIndex(len(doc)).drop(columns_row)]
        
        if doc is not None:
            doc = doc[file_atr['columns']].rename(columns = file_atr['rename_columns'])
    except ValueError as e:
        print(f'there is a mistake with the halls_inputs')
    
    doc.info()
    return doc    

